# Uno

프로그래밍기초 개인 프로젝트  
https://bitbucket.org/sangwooHan/uno

## 개발환경

- MAC OS X 10.11 (El Capitan)
- [Python3.5](http://www.python.org/)
- [virtualenv](https://python-guide.readthedocs.org/en/latest/dev/virtualenvs/#virtualenv)
- [Qt](https://www.qt.io/)
- [PyQt](https://riverbankcomputing.com/)

## 게임설명

[Uno](https://ko.wikipedia.org/wiki/%EC%9A%B0%EB%85%B8)