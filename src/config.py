# -*- coding: utf-8 -*-
import sys
import os

if getattr(sys, 'frozen', False):
    BASE_DIR = DATA_DIR = os.path.abspath(os.path.dirname(sys.executable))
    RES_DIR = os.path.join(BASE_DIR, '../Resources/') if sys.platform == 'darwin' else BASE_DIR
else:
    BASE_DIR = DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    RES_DIR = os.path.join(BASE_DIR, 'res')
