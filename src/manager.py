# -*- coding: utf-8 -*-
import os, json, copy
from models import *
from config import DATA_DIR

class Manager:

	_instance = None
	filename = None
	KEYS = ['name', 'total', 'score']

	def __new__(cls, filename='members.json'):
		path = os.path.join(DATA_DIR, filename)
		if not os.path.isfile(path):
			with open(path, 'w') as f:
				f.write('[]')

		if not cls._instance:
			cls._instance = super(Manager, cls).__new__(cls)
			cls._instance.filename = filename

		return cls._instance

	def get_player(self, name):
		filename = os.path.join(DATA_DIR, self.filename)
		keys = Manager.KEYS
		with open(filename, 'r') as f:
			players = json.load(f)

		for player in players:
			if player[keys[0]] == name:
				return Player(name, player[keys[1]], player[keys[2]])
		return Player(name)

	def update_history(self, player):
		filename = os.path.join(DATA_DIR, self.filename)
		keys = Manager.KEYS
		with open(filename, 'r') as f:
			users = json.load(f)

		for user in users.copy():
			if user[keys[0]] == player.name:
				users.remove(user)

		users.append(player.get_info())

		with open(filename, 'w') as f:
			f.write(json.dumps(users))

	def clear_history(self):
		filename = os.path.join(DATA_DIR, self.filename)
		os.remove(filename)

	def get_rank(self):
		filename = os.path.join(DATA_DIR, self.filename)
		keys = Manager.KEYS
		with open(filename, 'r') as f:
			users = json.load(f)

		users = sorted(users, key=lambda x: x[keys[2]], reverse=True)
		players = []
		for user in users:
			players.append(Player(user[keys[0]], user[keys[1]], user[keys[2]]))
		return players

class Ruler:

	CLOCK_WISE = 1
	COUNTER_CLOCK_WISE = 2

	DRAW_TWO = 1
	SKIP = 2
	REVERSE = 3
	WILD = 4
	WILD_DRAW_FOUR = 5
	GENERAL = 6

	def __init__(self, name, num):
		self._deck = Deck()
		self._player = GamePlayer(Manager().get_player(name))
		self._ai = []

		for i in range(num):
			self._ai.append(GamePlayer(Ai('ai_%d' % (i + 1))))

		self._deal_available = False
		self._direction = self.COUNTER_CLOCK_WISE		# 첫 게임시작 시 반시계방향
		self._first_turn = True
		self._selected_color = None

		self._order = 0									# player 부터
		self._is_skip = False
		self._is_deal_wild_draw_four = False
		self._is_deal_draw_two = False
		
	@property
	def deal_available(self):
		return self._deal_available

	@property
	def direction(self):
		return self._direction

	@property
	def order(self):
		return self._order

	def is_first_turn(self):
		return self._first_turn
	
	def is_deal_wild_draw_four(self):
		return self._is_deal_wild_draw_four
	
	def set_deal_wild_draw_four(self, value):
		self._is_deal_wild_draw_four = value
		
	
	def is_deal_draw_two(self):
		return self._is_deal_draw_two
	
	def set_deal_draw_two(self, value):
		self._is_deal_draw_two = value

	def set_no_first_turn(self):
		self._first_turn = False

	def set_skip(self):
		self._is_skip = True

	def start(self):
		deck = self._deck
		for _ in range(7):
			self._player.take(deck.give(True))
			for ai in self._ai:
				ai.take(deck.give())

		while True:
			card = deck.give(True)
			if self.is_wild_draw_four(card):
				deck.back(card)
				continue
			else:
				deck.take(card)
				break

	def next_order(self):
		if self.direction == self.COUNTER_CLOCK_WISE:
			if self._is_skip:
				self._is_skip = False
				self._order += 2
			else:
				self._order += 1

			if self._order >= len(self._ai) + 1:
				self._order -= len(self._ai) + 1
		else:
			if self._is_skip:
				self._is_skip = False
				self._order -= 2
			else:
				self._order -= 1

			if self._order < 0:
				self._order += len(self._ai) + 1
		return self._order
	
	def previous_order(self):
		if self.direction == self.COUNTER_CLOCK_WISE:
			order = self._order - 1
			if order == -1:
				order = len(self._ai)
		else:
			order = self._order + 1
			if order == len(self._ai) + 1:
				order = 0
		return order
	
	def get_player_card(self, index):
		return self._player.cards[index]

	def get_player_cards(self):
		return self._player.cards

	def get_ai_cards(self, index):
		return self._ai[index].cards
	
	def get_ai_card(self, index, index2):
		return self._ai[index].cards[index2]

	def get_top_card(self):
		deck = self._deck
		return deck.get_top_card()

	def get_opened_top_card(self):
		deck = self._deck
		return deck.get_opened_top_card()

	def reverse(self):
		self._direction = self.CLOCK_WISE \
			if self._direction == self.COUNTER_CLOCK_WISE \
			else self.COUNTER_CLOCK_WISE

	def switch_deal(self, value):
		self._deal_available = value

	def deal(self, card, ai_index=None):
		deck = self._deck
		if ai_index is None:
			player = self._player
		else:
			player = self._ai[ai_index]

		player.throw(card)
		deck.take(card)

		if self.is_draw_two(card):
			return self.DRAW_TWO
		if self.is_skip(card) or (len(self._ai) == 1 and self.is_reverse(card)):
			return self.SKIP
		if self.is_reverse(card):
			return self.REVERSE
		if self.is_wild(card):
			return self.WILD
		if self.is_wild_draw_four(card):
			return self.WILD_DRAW_FOUR
		return self.GENERAL

	def set_selected_color(self, color):
		self._selected_color = color

	def get_selected_color(self):
		return self._selected_color

	def is_wild(self, card=None):
		if card is None:
			card = self._deck.get_opened_top_card()
		return card.color == Card.COLORS[4] and card.rank == Card.SPEICAL[0]

	def is_wild_draw_four(self, card=None):
		if card is None:
			card = self._deck.get_opened_top_card()
		return card.color == Card.COLORS[4] and card.rank == Card.SPEICAL[1]

	def is_reverse(self, card=None):
		if card is None:
			card = self._deck.get_opened_top_card()
		return card.rank == Card.GENERAL[12]

	def is_skip(self, card=None):
		if card is None:
			card = self._deck.get_opened_top_card()
		return card.rank == Card.GENERAL[10]

	def is_draw_two(self, card=None):
		if card is None:
			card = self._deck.get_opened_top_card()
		return card.rank == Card.GENERAL[11]

	def is_same_color(self, card):
		opened_card = self._deck.get_opened_top_card()
		return card.color == opened_card.color

	def is_same_rank(self, card):
		opened_card = self._deck.get_opened_top_card()
		return card.rank == opened_card.rank

	def player_deck_check(self):
		for card in self._player.cards:
			if self.is_wild_draw_four(card):
				continue
			if self.is_same_color(card) or self.is_same_rank(card):
				return True
		return False

	def take_card(self, ai_index=None):
		deck = self._deck
		if ai_index is None:
			player = self._player
			show = True
		else:
			player = self._ai[ai_index]
			show = False
 
		card = deck.give(show)
		
		player.take(card)
		return card

	def is_finish(self):
		if len(self._player.cards) == 0:
			return True

		for ai in self._ai:
			if len(ai.cards) == 0:
				return True

		return False
	
	def can_deal(self, ai_index, free_pass=False):
		cards = self._ai[ai_index].cards
		same_color_or_rank = False
		index = -1
		for i in range(len(cards)):
			if self.is_wild_draw_four(cards[i]):
				continue
			
			if self.is_wild(cards[i]):
				return i
			
			if self.is_same_color(cards[i]):
				same_color_or_rank = True
				index = i
				
			if self.is_same_rank(cards[i]):
				same_color_or_rank = True
				index = i
				
		if same_color_or_rank:
			return index
		
		for i in range(len(cards)):
			if self.is_wild_draw_four(cards[i]):
				return i
			
		if free_pass:
			return random.randint(0, len(cards)-1)
		
		return None
	
	def wild_deal(self, ai_index, color):
		cards = self._ai[ai_index].cards
		for i in range(len(cards)):
			if cards[i].color == color:
				return i
		return self.can_deal(ai_index)
		
	def can_defend_draw_two(self, ai_index):
		cards = self._ai[ai_index].cards
		for i in range(len(cards)):
			if cards[i].rank == Card.GENERAL[11]:
				return i
		return None
		
	def pick_random_color(self, index):
		cards = self._ai[index].cards
		colors = copy.copy(Card.COLORS)
		colors.pop()
		red_num = 0
		yellow_num = 0
		green_num = 0
		blue_num = 0
		
		for card in cards:
			if card.color == Card.COLORS[0]:
				red_num += 1
			elif card.color == Card.COLORS[1]:
				yellow_num += 1
			elif card.color == Card.COLORS[2]:
				green_num += 1
			elif card.color == Card.COLORS[3]:
				blue_num += 1
				
		max_num = max(red_num, yellow_num, green_num, blue_num)
		
		if max_num == red_num:
			return Card.COLORS[0]
		
		if max_num == yellow_num:
			return Card.COLORS[1]
		
		if max_num == green_num:
			return Card.COLORS[2]
		
		if max_num == blue_num:
			return Card.COLORS[3]
	
	def get_score(self):
		if self._player.cards != []:
			return 0
		return 1
	
	def update_score(self):
		score = self.get_score()
		player = self._player.player
		player.score += score
		Manager().update_history(player)