# -*- coding: utf-8 -*-
import copy, os

from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from custom import *

from manager import *
from models import *
from config import RES_DIR

class Uno(QMainWindow):

	def __init__(self):
		super().__init__()
		self.setGeometry(300, 150, 300, 300)
		self.setWindowTitle('UNO')
		self.show()
		Main(self)

	def center(self):
		screen = QDesktopWidget().screenGeometry()
		size = self.geometry()
		self.move((screen.width()-size.width())/2,
			(screen.height()-size.height())/2)

class Main(QWidget):

	def __init__(self, parent=None):
		super().__init__(parent)
		self.parent = parent
		uic.loadUi(os.path.join(RES_DIR, 'layout/main.ui'), self).show()
		self.parent.resize(self.width(), self.height())

	@pyqtSlot()
	def show_rank(self):
		RankDialog(self).show()

	@pyqtSlot()
	def start(self):
		type_ = None	# 1 - 2인, 2 - 3인, 3 - 4인
		if self.radio_button.isChecked():
			type_ = GameView.TWO_PLAY
		elif self.radio_button_2.isChecked():
			type_ = GameView.THREE_PLAY
		else:
			type_ = GameView.FOUR_PLAY

		nickname = self.nickname_edit.text()
		if nickname.strip(' ') == '':
			QMessageBox.information(self, "Empty Field", "닉네임을 설정하세요")
		else:
			GameView(type_, self.nickname_edit.text(), self.parent)
			self.setParent(None)

class RankDialog(QDialog):

	def __init__(self, parent=None):
		super().__init__(parent)
		self.parent = parent
		uic.loadUi(os.path.join(RES_DIR, 'layout/rank.ui'), self).show()
		self.setWindowTitle('Rank')
		self.set_list()

	def set_list(self):
		manager = Manager()
		players = manager.get_rank()

		rank = 1
		for player in players:
			item = QListWidgetItem()
			widget = RankListItemWidget()
			widget.rank = rank
			widget.nickname = player.name
			widget.total = player.total
			widget.score = player.score
			item.setSizeHint(widget.sizeHint())
			self.list.addItem(item)
			self.list.setItemWidget(item, widget)
			rank += 1

class SelectingColorDialog(QDialog):

	def __init__(self, parent=None):
		super().__init__(parent)
		self.parent = parent
		uic.loadUi(os.path.join(RES_DIR, 'layout/select_color.ui'), self).show()
		self.setWindowTitle('Color')

		self.red_label.setAutoFillBackground(True)
		self.red_label.setPalette(QPalette(QColor(255, 0, 0)))

		self.yellow_label.setAutoFillBackground(True)
		self.yellow_label.setPalette(QPalette(QColor(255, 228, 0)))

		self.green_label.setAutoFillBackground(True)
		self.green_label.setPalette(QPalette(QColor(29, 219, 22)))

		self.blue_label.setAutoFillBackground(True)
		self.blue_label.setPalette(QPalette(QColor(1, 0, 255)))

	@pyqtSlot()
	def ok(self):
		self.close()

	def closeEvent(self, event):
		if self.red.isChecked():
			self.parent.select_color(Card.COLORS[0])
		elif self.yellow.isChecked():
			self.parent.select_color(Card.COLORS[1])
		elif self.green.isChecked():
			self.parent.select_color(Card.COLORS[2])
		else:
			self.parent.select_color(Card.COLORS[3])
			
class ResultDialog(QDialog):
	
	def __init__(self, score, parent=None):
		super().__init__(parent)
		self.parent = parent
		uic.loadUi(os.path.join(RES_DIR, 'layout/result_dialog.ui'), self).show()
		self.setWindowTitle('Result')
		
		if score == 0:
			self.message_label.setText('패배하셨습니다')
		else:
			self.message_label.setText('승리하셨습니다')
			
	@pyqtSlot()
	def ok(self):
		self.close()
		
	def closeEvent(self, event):
		self.parent.game_exit()

class GameView(QWidget):

	TWO_PLAY = 1
	THREE_PLAY = 2
	FOUR_PLAY = 3

	CARD_SIZES = (QSize(60, 100), QSize(100, 60)) # 카드 크기 세로형, 가로	

	ARROW_SIZE = QSize(30, 30)	# 화살표 크기
	
	COLOR_SIZE = QRect(255, 340, 30, 30) 		# 색깔 지정 표 위치 및 크기
	COLOR_RED = QPalette(QColor(255, 0, 0))
	COLOR_YELLOW = QPalette(QColor(255, 228, 0))
	COLOR_GREEN = QPalette(QColor(29, 219, 22))
	COLOR_BLUE = QPalette(QColor(1, 0, 255))
	

	delta = 25	# 카드 간격

	POS = {					# 카드 첫 시작 위치
		'deck': QPoint(350, 230),
		'opened_deck': QPoint(240, 230),
		'player': QPoint(220, 430),
		'ai': [(QPoint(220, 30), QPoint(60, 180), QPoint(510, 180)),	# Two play
			(QPoint(220, 30), QPoint(60, 180)),							# Three play
			(QPoint(510, 180), QPoint(220, 30), QPoint(60, 180))]		# Four play
	}

	ARROW_POS = [(QPoint(310, 370), QPoint(310, 150)),								# Two play
		(QPoint(310, 370), QPoint(310, 150), QPoint(180, 250)),						# Three play
		(QPoint(310, 370), QPoint(460, 250), QPoint(310, 150), QPoint(180, 250))]	# Four play

	TRANSFORMS = [(QTransform().rotate(180),),											# Two play
		(QTransform().rotate(180), QTransform().rotate(90)),							# Three play
		(QTransform().rotate(270), QTransform().rotate(180), QTransform().rotate(90))]	# Four play

	ANIMATED_SPEED = 150		# 애니메이션 속도 ms단위

	FOCUS_DELTA = 25			# Mouse hover 되었을 시 움직일 y길이
	
	class AiWorker(QThread):
		
		take_card = pyqtSignal(int, int)
		throw_card = pyqtSignal(int, int, bool)
		ai_play_finished = pyqtSignal()
		each_ai_play_started = pyqtSignal(int)
		defend_draw_two = pyqtSignal()
		
		def __init__(self, ai_labels, ruler, *args, **kwargs):
			super().__init__(*args, **kwargs)
			self.ai_labels = ai_labels
			self.ruler = ruler
			
		def __del__(self):
			self.wait()
			
		def run(self):
			while True:
				QThread.msleep(500)	# 1초 간격으로 ai play
				
				order = self.ruler.next_order()
				index = order - 1
				if order == 0:		# 플레이어 차례일 때 쓰레드 종료
					break
				self.each_ai_play_started.emit(index)
				
				QThread.msleep(500)
				if self.ruler.is_finish():
					break
				
				if self.ruler.is_wild_draw_four():
					if self.ruler.is_deal_wild_draw_four():
						self.deal(index, True)
					else:
						self.take_card.emit(index, 4)
						self.ruler.set_deal_wild_draw_four(True)
				
				elif self.ruler.is_wild():
					self.deal(index, color=self.ruler.get_selected_color())
					
				elif self.ruler.is_draw_two():
					if self.ruler.is_deal_draw_two():
						self.deal(index)
					else:
						card_index = self.ruler.can_defend_draw_two(index)
						if card_index is not None:
							self.ruler.set_deal_draw_two(True)
							self.throw_card.emit(index, card_index, True)
							
							pre_order = self.ruler.previous_order()
							
							if pre_order == 0:
								self.defend_draw_two.emit()
							else:
								self.ruler.set_deal_draw_two(True)
								self.take_card.emit(pre_order-1, 2)
						else:
							self.ruler.set_deal_draw_two(True)
							self.take_card.emit(index, 2)
				else:
					self.deal(index)
				
			self.ai_play_finished.emit()
			
		def deal(self, index, free_pass=False, color=None):
			if color is not None:
				card_index = self.ruler.wild_deal(index, color)
			else:
				card_index = self.ruler.can_deal(index, free_pass)
				
			if card_index is not None:
				self.throw_card.emit(index, card_index, False)
			else:
				self.take_card.emit(index, 1)

	def __init__(self, type_, nickname, parent=None):
		super().__init__(parent)

		uic.loadUi(os.path.join(RES_DIR, 'layout/game.ui'), self).show()
		self.deck_label = MyLabel(self)
		self.opened_deck_label = QLabel(self)
		self.arrow_label = QLabel(self)
		self.arrow_img = QPixmap(':img/arrow.png')\
			.scaled(self.ARROW_SIZE, transformMode=Qt.SmoothTransformation)

		self.deck_label.clicked.connect(self.deck_click)

		self.ai_labels = []
		self.player_labels = []
		self.parent = parent
		self.num = type_
		self.parent.resize(self.size())

		self.setAutoFillBackground(True)
		self.setPalette(QPalette(QColor(29, 139, 21)))	# Green
		
		self.color_label = QLabel(self)
		self.color_label.setGeometry(self.COLOR_SIZE)
		self.color_label.setAutoFillBackground(True)

		self.deck_label.setGeometry(QRect(self.POS['deck'], self.CARD_SIZES[0]))
		self.opened_deck_label.setGeometry(QRect(self.POS['deck'], self.CARD_SIZES[0]))

		self.deck_label.show()

		for _ in range(self.num):
			self.ai_labels.append([])

		self.ruler = Ruler(nickname, self.num)
		
		self.ai_worker = self.AiWorker(self.ai_labels, self.ruler)
		self.ai_worker.take_card.connect(self.ai_take_card)
		self.ai_worker.throw_card.connect(self.ai_throw_card)
		self.ai_worker.ai_play_finished.connect(self.ai_play_finish)
		self.ai_worker.each_ai_play_started.connect(self.each_ai_play_start)
		self.ai_worker.defend_draw_two.connect(self.reflect_draw_two)

		self.start()
		
	def show_color_label(self):
		color = self.ruler.get_selected_color()
		if color == Card.COLORS[0]:
			palette = self.COLOR_RED
		elif color == Card.COLORS[1]:
			palette = self.COLOR_YELLOW
		elif color == Card.COLORS[2]:
			palette = self.COLOR_GREEN
		else:
			palette = self.COLOR_BLUE
			
		self.color_label.setPalette(palette)
		self.color_label.show()
		
	def hide_color_label(self):
		self.color_label.hide()

	def start(self):
		self.ruler.start()

		self.start_animation_group = QSequentialAnimationGroup()
		POS = self.POS
		CARD_SIZES = self.CARD_SIZES

		card = self.ruler.get_top_card()

		if card is not None:
			self.deck_label.setPixmap(card.img)
		else:
			self.deck_label.hide()


		player_cards = self.ruler.get_player_cards()
		ai_cards = []

		for i in range(self.num):
			ai_cards.append(self.ruler.get_ai_cards(i))

		for i in range(7):
			label = MyLabel(self)
			label.setPixmap(player_cards[i].img)

			label.hovered.connect(self.hover)
			label.leaved.connect(self.leave)
			label.clicked.connect(self.click)

			self.player_labels.append(label)

			point = copy.copy(POS['player'])
			point.setX(point.x()+i*self.delta)
			animate = self._get_animation(label, POS['deck'], point, CARD_SIZES[0])
			animate.started.connect(self.animation_start)
			self.start_animation_group.addAnimation(animate)

			for j in range(self.num):
				label = MyLabel(self)
				label.setPixmap(self._get_correct_card_img(ai_cards[j][i].img, j))

				self.ai_labels[j].append(label)

				point = self._get_correct_pos(i*self.delta, j)
				animate = self._get_animation(label, POS['deck'], point, self._get_correct_card_size(j))
				animate.started.connect(self.animation_start)
				self.start_animation_group.addAnimation(animate)

		self.opened_deck_label.setPixmap(self.ruler.get_opened_top_card().img)
		animate = self._get_animation(self.opened_deck_label,
			POS['deck'], POS['opened_deck'], CARD_SIZES[0])
		animate.started.connect(self.animation_start)

		self.start_animation_group.addAnimation(animate)

		self.start_animation_group.start()
		self.start_animation_group.finished.connect(self.animation_group_finish)

	def select_color(self, color):
		self.ruler.set_selected_color(color)
		self.show_color_label()
		self.ai_worker.start()

	def finish(self):
		score = self.ruler.get_score()
		self.ruler.update_score()
		ResultDialog(score, self).show()
		
	def game_exit(self):
		Main(self.parent)
		self.setParent(None)
		
	def is_x_direction(self, ai_index):
		if self.num == 1:
			return True
		if self.num == 2:
			return ai_index == 0
		
		return ai_index == 1

	def _get_correct_card_size(self, index):
		if self.num == 1:
			return self.CARD_SIZES[0]
		elif self.num == 2:
			return self.CARD_SIZES[index]
		else:
			return self.CARD_SIZES[1-index%2]

	def _get_correct_card_img(self, img, index):
		return img.transformed(self.TRANSFORMS[self.num-1][index])

	def _get_correct_pos(self, delta, index):
		point = copy.copy(self.POS['ai'][self.num-1][index])
		if self.num == 1 or self.num == 2:
			if index == 0:
				point.setX(point.x()+delta)
			else:
				point.setY(point.y()+delta)
		else:
			if index == 1:
				point.setX(point.x()+delta)
			else:
				point.setY(point.y()+delta)
		return point

	def _get_animation(self, target, point1, point2, size1, size2=None):
		if size2 is None:
			size2 = size1

		animate = MyAnimation(target, b'geometry')
		animate.setDuration(self.ANIMATED_SPEED)
		animate.setStartValue(QRect(point1, size1))
		animate.setEndValue(QRect(point2, size2))
		
		return animate

	def _get_throwing_animation_group(self, index, ai_index=None):
		animation_group = QParallelAnimationGroup()
		
		if ai_index is not None:
			labels = self.ai_labels[ai_index]
			card = self.ruler.get_ai_card(ai_index, index)
			card.flip()
		else:
			labels = self.player_labels
			
		label = labels.pop(index)
		label.raise_()
		
		if ai_index is not None:
			label.resize(self.opened_deck_label.size())
			label.setPixmap(card.img)
		
		animate = self._get_animation(label, label.pos(), 
					self.opened_deck_label.pos(), label.size())
		animate.finished.connect(self.animation_finish)
		
		animation_group.addAnimation(animate)
		
		delta = self.delta // 2
		
		for i in range(0, index):
			label= labels[i]	
			
			if ai_index is None:
				point = QPoint(label.x()+delta, label.y())
			else:
				if self.is_x_direction(ai_index):
					point = QPoint(label.x()+delta, label.y())
				else:
					point = QPoint(label.x(), label.y()+delta)
				
			animate = self._get_animation(label, label.pos(), 
							point, label.size())
			
			animation_group.addAnimation(animate)
			
		for i in range(index, len(labels)):
			label = labels[i]
			
			if ai_index is None:
				point = QPoint(label.x()-delta, label.y())
			else:
				if self.is_x_direction(ai_index):
					point = QPoint(label.x()-delta, label.y())
				else:
					point = QPoint(label.x(), label.y()-delta)
			
			animate = self._get_animation(label, label.pos(), 
							point, label.size())
			
			animation_group.addAnimation(animate)
		
		return animation_group
		
	def _get_taking_animation_group(self, num, ai_index=None):
		animation_group = QSequentialAnimationGroup()
		
		if ai_index is None:
			labels = self.player_labels
		else:
			labels = self.ai_labels[ai_index]
		
		for i in range(num):
			delta = num*(self.delta//2) - (i+1)*self.delta
			if ai_index is not None:
				label = QLabel(self)
				card = self.ruler.take_card(ai_index)
				img = self._get_correct_card_img(card.img, ai_index)
				
				if self.is_x_direction(ai_index):
					point = QPoint(labels[-(1+i)].x()-delta, labels[-(1+i)].y())
				else:
					point = QPoint(labels[-(1+i)].x(), labels[-(1+i)].y()-delta)
			else:
				label = MyLabel(self)
				card = self.ruler.take_card()
				img = card.img
				
				point = QPoint(labels[-(1+i)].x()-delta, labels[-(1+i)].y())
				
				label.hovered.connect(self.hover)
				label.leaved.connect(self.leave)
				label.clicked.connect(self.click)
				
			label.setGeometry(QRect(point, labels[-1].size()))
			label.setPixmap(img)
			labels.append(label)
			
			animate = self._get_animation(label, self.POS['deck'], point, labels[-1].size())
			animate.started.connect(self.animation_start)
			
			animation_group.addAnimation(animate)
			
		animation_group2 = QParallelAnimationGroup()
		
		delta = num * (self.delta//2)
		
		for i in range(len(labels)-num):
			label = labels[i]
			
			if ai_index is None:
				point = QPoint(label.x()-delta, label.y())
			else:
				if self.is_x_direction(ai_index):
					point = QPoint(label.x()-delta, label.y())
				else:
					point = QPoint(label.x(), label.y()-delta)
					
			animate = self._get_animation(label, label.pos(), point, label.size())
			animation_group2.addAnimation(animate)
			
		animation_group.addAnimation(animation_group2)
		return animation_group
		
	def deal(self, card, index):
		ruler = self.ruler
		result = ruler.deal(card)

		if result == Ruler.DRAW_TWO:
			ruler.set_deal_draw_two(False)
		elif result == Ruler.SKIP:
			ruler.set_skip()
		elif result == Ruler.REVERSE:
			ruler.reverse()
		elif result == Ruler.WILD:
			if not ruler.is_finish():
				SelectingColorDialog(self).show()
		elif result == Ruler.WILD_DRAW_FOUR:
			ruler.set_deal_wild_draw_four(False)

		self.deal_animation_group = self._get_throwing_animation_group(index)
		self.deal_animation_group.start()

		ruler.set_no_first_turn()
		ruler.switch_deal(False)

		if ruler.is_finish():
			self.finish()
		elif result != Ruler.WILD:
			self.ai_worker.start()
			
	def defend_draw_two(self, card, index):
		
		self.ruler.deal(card)
		
		self.deal_animation_group = self._get_throwing_animation_group(index)
		self.deal_animation_group.start()
		
		pre_order = self.ruler.previous_order()
		index = pre_order - 1
		
		self.defend_draw_two_animation_group = self._get_taking_animation_group(2, index)
		self.defend_draw_two_animation_group.start()
		
		self.ruler.set_deal_draw_two(True)
		self.ruler.switch_deal(False)
		
		self.ai_worker.start()
			
	@pyqtSlot()
	def reflect_draw_two(self):
		self.reflect_draw_two_animation_group = self._get_taking_animation_group(2)
		self.reflect_draw_two_animation_group.start()
		self.ruler.set_deal_draw_two(True)

	@pyqtSlot(int, int)
	def ai_take_card(self, index, num):			
		self.take_animation_group = self._get_taking_animation_group(num, index)
		self.take_animation_group.start()
		
	@pyqtSlot(int, int, bool)
	def ai_throw_card(self, index, card_index, defend_draw_two):		
		self.throw_animation_group = self._get_throwing_animation_group(card_index, index)
		self.throw_animation_group.start()
		
		result = self.ruler.deal(self.ruler.get_ai_card(index, card_index), index)
		
		if result == Ruler.REVERSE:
			self.ruler.reverse()
		elif result == Ruler.SKIP:
			self.ruler.set_skip()
		elif result == Ruler.WILD:
			self.ruler.set_selected_color(self.ruler.pick_random_color(index))
			self.show_color_label()
		elif result == Ruler.WILD_DRAW_FOUR:
			self.ruler.set_deal_wild_draw_four(False)
		elif result == Ruler.DRAW_TWO:
			if not defend_draw_two:
				self.ruler.set_deal_draw_two(False)
				
	@pyqtSlot(int)
	def each_ai_play_start(self, index):
		img = self._get_correct_card_img(self.arrow_img, index)
		point = self.ARROW_POS[self.num-1][index+1]
		self.arrow_label.setPixmap(img)
		self.arrow_label.move(point)
		
	@pyqtSlot()
	def ai_play_finish(self):
		point = self.ARROW_POS[self.num-1][0]
		self.arrow_label.setPixmap(self.arrow_img)
		self.arrow_label.move(point)
		
		if self.ruler.is_finish():
			self.finish()
			
		else:
			if self.ruler.is_wild_draw_four() and not self.ruler.is_deal_wild_draw_four():
				self.player_wild_draw_four_animation_group = self._get_taking_animation_group(4)
				self.player_wild_draw_four_animation_group.start()
				
				self.ruler.set_deal_wild_draw_four(True)
				self.ai_worker.start()
				
			else:
				self.ruler.switch_deal(True)
		
	@pyqtSlot('QObject')
	def animation_start(self, label):
			label.show()

	@pyqtSlot('QObject')
	def animation_finish(self, label):
		self.opened_deck_label.setPixmap(label.pixmap())
		label.setParent(None)

	@pyqtSlot('QObject')
	def hover(self, label):
		if not self.ruler.deal_available:
			return

		for i in range(len(self.player_labels)):
			if label == self.player_labels[i]:
				label.move(label.x(), label.y()-self.FOCUS_DELTA)

	@pyqtSlot('QObject')
	def leave(self, label):
		if not self.ruler.deal_available:
			return

		for i in range(len(self.player_labels)):
			if label == self.player_labels[i]:
				label.move(label.x(), self.POS['player'].y())
				if i != len(self.player_labels)-1:
					label.stackUnder(self.player_labels[i+1])

	@pyqtSlot('QObject')
	def click(self, label):
		ruler = self.ruler
		if not ruler.deal_available:
			return

		for i in range(len(self.player_labels)):
			if label == self.player_labels[i]:
				card = ruler.get_player_card(i)

				if ruler.is_wild_draw_four(card):
					if not ruler.player_deck_check():
						self.deal(card, i)

				elif ruler.is_wild(card):
					if ruler.is_draw_two():
						if ruler.is_deal_draw_two():
							self.deal(card, i)
					else:
						self.deal(card, i)

				elif ruler.is_first_turn():
					if ruler.is_wild_draw_four() or ruler.is_wild():
						self.deal(card, i)
					elif ruler.is_same_color(card) or ruler.is_same_rank(card):
						self.deal(card, i)

				else:
					if ruler.is_wild():
						color = ruler.get_selected_color()
						if card.color == color:
							self.deal(card, i)
					elif ruler.is_draw_two():
						if ruler.is_deal_draw_two():
							if ruler.is_same_color(card) or ruler.is_same_rank(card):
								self.deal(card, i)
								
						elif ruler.is_draw_two(card):
							self.defend_draw_two(card, i)
							
					elif ruler.is_same_color(card) or ruler.is_same_rank(card):
						self.deal(card, i)
					elif ruler.is_wild_draw_four():
						self.deal(card, i)
				break

	@pyqtSlot('QObject')
	def deck_click(self, label):
		ruler = self.ruler
		if not ruler.deal_available:
			return
		
		if ruler.is_first_turn():
			num = 1
		elif ruler.is_draw_two() and not ruler.is_deal_draw_two():
			num = 2
			ruler.set_deal_draw_two(True)
		else:
			num = 1

		self.deck_click_animation_group = self._get_taking_animation_group(num)
		self.deck_click_animation_group.start()
		ruler.switch_deal(False)
		ruler.set_no_first_turn()
		self.ai_worker.start()

	@pyqtSlot()
	def animation_group_finish(self):
		self.arrow_label.setPixmap(self.arrow_img)
		self.arrow_label.setGeometry(QRect(self.ARROW_POS[self.num-1][0], self.arrow_img.size()))
		self.arrow_label.show()
		self.ruler.switch_deal(True)