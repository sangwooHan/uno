"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup
import sys

APP = ['src/run.py']
DATA_FILES = ['res/layout']
OPTIONS = {'argv_emulation': True, 'includes': ['sip', 'PyQt5']}

if sys.platform == 'darwin':
	extra_options = dict(
			data_files=DATA_FILES,
			setup_requires=['py2app'],
			app=APP,
			options=dict(py2app=OPTIONS),
	)
elif sys.platform == 'win32':
	import py2exe
	APP = [{'script': 'run.py'}]
	OPTIONS = {'bundle_files': 1, 'compressed': True, 'includes': ['sip']}
	extra_options = dict(
			data_files=[('layout', ['../res/layout/game.ui', '../res/layout/main.ui', '../res/layout/rank.ui', 
				'../res/layout/result_dialog.ui', '../res/layout/select_color.ui']),
				('platforms', [r'C:\Python34\Lib\site-packages\PyQt5\plugins\platforms\qwindows.dll']),
				('', [r'C:\windows\syswow64\msvcp100.dll', r"C:\windows\syswow64\msvcr100.dll"])],
			windows=APP,
			options=dict(p2exe=OPTIONS),
	)
else:
	import ez_setup
	ez_setup.use_setuptools()

	extra_options = dict(
			scripts=APP,
	)

setup(
    **extra_options
)
