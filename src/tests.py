# -*- coding: utf-8 -*-
import unittest
from manager import Manager
from models import Player, Deck

class ManagerTest(unittest.TestCase):

	def setUp(self):
		self.manager = Manager('test_members.json')

	def tearDown(self):
		self.manager.clear_history()

	def test_get_user(self):
		manager = self.manager
		p = manager.get_player('han')
		p2 = Player('han')
		self.assertEqual(p.__dict__, p2.__dict__)

	def test_update_history(self):
		manager = self.manager
		p = Player('han', 10, 3)
		manager.update_history(p)
		p2 = manager.get_player('han')
		self.assertEqual(p.__dict__, p2.__dict__)

	def test_get_rank(self):
		manager = self.manager
		p1 = Player('han', 10, 3)
		p2 = Player('sang', 13, 1)
		p3 = Player('woo', 12, 5)
		p4 = Player('chicken', 20, 12)
		p5 = Player('cow', 22, 11)
		players = [p4.get_info(), p5.get_info(), p3.get_info(), p1.get_info(), p2.get_info()]
		manager.update_history(p1)
		manager.update_history(p2)
		manager.update_history(p3)
		manager.update_history(p4)
		manager.update_history(p5)
		users = manager.get_rank()
		players2 = []
		for user in users:
			players2.append(user.get_info())
		self.assertEqual(players, players2)

class DeckTest(unittest.TestCase):

	def setUp(self):
		self.deck = Deck()

	def test_valid_deck(self):
		deck = self.deck
		self.assertEqual(len(deck.cards), 108)