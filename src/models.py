# -*- coding: utf-8 -*-
import os, random

from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QSize, Qt
from config import RES_DIR

class Ai:

	def __init__(self, name):
		self._name = name

	@property
	def name(self):
		return self._name

class Player(Ai):

	def __init__(self, name, total=0, score=0):
		super().__init__(name)
		self._total = total
		self._score = score

	@property
	def total(self):
		return self._total

	@property
	def score(self):
		return self._score
	
	@score.setter
	def score(self, value):
		self._score = value
		self._total += 1

	def get_info(self):
		return {
			'name': self.name,
			'total': self.total,
			'score': self.score
		}

class GamePlayer:

	def __init__(self, player):
		self.player = player
		self._cards = []

	@property
	def cards(self):
		return self._cards.copy()

	def take(self, card):
		self._cards.append(card)

	def throw(self, card):
		try:
			self._cards.remove(card)
		except:
			pass

class Card:

	COLORS = ['RED', 'YELLOW', 'GREEN', 'BLUE', 'BLACK']
	GENERAL = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
			 'skip', 'draw_two', 'reverse']
	SPEICAL = ['wild', 'wild_draw_four']

	def __init__(self, color, rank, face_up=True, size=QSize(60, 100)):

		self.color = color
		self.rank = rank
		self.face_up = face_up
		self._back = QPixmap(':img/back.png')\
			.scaled(size, transformMode=Qt.SmoothTransformation)

		if rank in Card.SPEICAL:
			self._img = QPixmap(':img/%s.png' % (rank))\
				.scaled(size, transformMode=Qt.SmoothTransformation)
		else:
			self._img = QPixmap(':img/%s_%s.png' % (color.lower(), rank))\
				.scaled(size, transformMode=Qt.SmoothTransformation)

	# def __eq__(self, other):
	# 	return isinstance(other, self.__class__) \
	# 		and self.color == other.color \
	# 		and self.rank == other.rank

	def flip(self):
		self.face_up = not self.face_up

	def __str__(self):
		return '%s %s' % (self.color, self.rank)

	@property
	def img(self):
		return self._img if self.face_up else self._back

class Deck:

	def __init__(self):
		self.fresh_deck()

	def fresh_deck(self):
		self._cards = []
		self._opened_cards = []
		colors = Card.COLORS.copy()
		general = Card.GENERAL.copy()

		colors.remove(Card.COLORS[4])
		general.remove(Card.GENERAL[0])

		for color in colors:
			self._cards.append(Card(color, Card.GENERAL[0], False))
			for _ in range(2):
				for g in general:
					self._cards.append(Card(color, g, False))

		for _ in range(4):
			for s in Card.SPEICAL:
				self._cards.append(Card(Card.COLORS[4], s, False))

		random.shuffle(self._cards)

	def existed_back(self):
		return self._cards != []

	def existed_opened(self):
		return self._opened_cards != []

	def get_opened_top_card(self):
		try:
			return self._opened_cards[-1]
		except:
			return None

	def get_top_card(self):
		try:
			return self._cards[-1]
		except:
			return None

	def give(self, flip=False):
		try:
			card = self._cards.pop()
			if flip:
				card.flip()
			return card
		except:
			last = self._opened_cards.pop()
			self._cards = self._opened_cards
			random.shuffle(self._cards)
			
			for card in self._cards:
				card.flip()
				
			self._opened_cards = [last]
			card = self._cards.pop()

			if flip:
				card.flip()
			return card

	def take(self, card):
		self._opened_cards.append(card)

	# 첫 패가 wild draw four일 경우
	def back(self, card):
		rand = random.randint(0, len(self._cards))
		card.flip()
		self._cards.insert(rand, card)