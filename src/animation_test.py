# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Frame(QWidget):

	def __init__(self, parent=None):
		super().__init__(parent)
		self.resize(600, 500)

		self.animation_group = QSequentialAnimationGroup()
		self.animation_group2 = QParallelAnimationGroup()
# 		animation_group2 = QParallelAnimationGroup()
		self.label = QLabel('Hello', self)
		self.label2 = QLabel('hi', self)
		self.label3 = QLabel('bobo', self)
		self.label4 = QLabel('zzzzzz', self)
		
		animate = QPropertyAnimation(self.label, b'geometry')
		animate.setDuration(800)
		animate.setStartValue(QRect(0, 0, 100, 30))
		animate.setEndValue(QRect(250, 250, 300, 30))
		self.animation_group.addAnimation(animate)
		
		animate = QPropertyAnimation(self.label2, b'geometry')
		animate.setDuration(800)
		animate.setStartValue(QRect(0, 0, 100, 30))
		animate.setEndValue(QRect(250, 180, 300, 30))
		self.animation_group.addAnimation(animate)
		
		animate = QPropertyAnimation(self.label3, b'geometry')
		animate.setDuration(800)
		animate.setStartValue(QRect(0, 0, 100, 30))
		animate.setEndValue(QRect(250, 90, 300, 30))
		self.animation_group2.addAnimation(animate)
		
		animate = QPropertyAnimation(self.label4, b'geometry')
		animate.setDuration(800)
		animate.setStartValue(QRect(0, 0, 100, 30))
		animate.setEndValue(QRect(300, 90, 300, 30))
		self.animation_group2.addAnimation(animate)
		
# 		self.animation_group.addAnimation(animation_group2)
# 		self.animation_group.addAnimation(animate)
# 		self.animation_group.start()

		self.animation_group.start()
		self.animation_group2.start()

if __name__ == '__main__':
	app = QApplication(sys.argv)
	frame = Frame()
	frame.show()
	sys.exit(app.exec_())