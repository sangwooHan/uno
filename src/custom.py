# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QLayout
from PyQt5.QtCore import QPropertyAnimation, pyqtSignal, QAbstractAnimation,\
	QParallelAnimationGroup, QSequentialAnimationGroup

class RankListItemWidget(QWidget):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		hbox_layout = QHBoxLayout()
		self._rank_label = QLabel()
		self._nickname_label = QLabel()
		self._total_label = QLabel()
		self._score_label = QLabel()

		# self._rank_label.setFixedHeight(20)
		# self._nickname_label.setFixedHeight(20)
		# self._total_label.setFixedHeight(20)
		# self._score_label.setFixedHeight(20)

		# self._rank_label.resize(self._rank_label.width(), 30)
		# self._nickname_label.resize(self._nickname_label.width(), 30)
		# self._nickname_label.resize(self._nickname_label.width(), 30)
		# self._score_label.resize(self._score_label.width(), 30)

		hbox_layout.addWidget(self._rank_label)
		hbox_layout.addWidget(self._nickname_label)
		hbox_layout.addWidget(self._total_label)
		hbox_layout.addWidget(self._score_label)

		# hbox_layout.addStretch(0)
		# hbox_layout.setSizeConstraint(QLayout.SetFixedSize)

		self.setLayout(hbox_layout)

	@property
	def rank(self):
		return self._rank_label.text()
	
	@rank.setter
	def rank(self, value):
		self._rank_label.setText(str(value))

	@property
	def nickname(self):
		return self._nickname_label.text()

	@nickname.setter
	def nickname(self, value):
		self._nickname_label.setText(str(value))

	@property
	def total(self):
		return self._total_label.text()

	@total.setter
	def total(self, value):
		self._total_label.setText(str(value))

	@property
	def score(self):
		return self._score_label.text()

	@score.setter
	def score(self, value):
		self._score_label.setText(str(value))

class MyAnimation(QPropertyAnimation):

	started = pyqtSignal('QObject')
	finished = pyqtSignal('QObject')

	def updateState(self, new, old):
		super().updateState(new, old)
		if old == QAbstractAnimation.Stopped and new == QAbstractAnimation.Running:
			self.started.emit(self.targetObject())
		elif old == QAbstractAnimation.Running and new == QAbstractAnimation.Stopped:
			self.finished.emit(self.targetObject())

class MySequentialAnimationGroup(QSequentialAnimationGroup):
	
	next = pyqtSignal(int, int)
	
	def __init__(self, index, num, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.index = index
		self.num = num
		
	def updateState(self, new, old):
		if old == QAbstractAnimation.Running and new == QAbstractAnimation.Stopped:
			self.num -= 1
			if self.num != 0:
				self.next.emit(self.index, self.num)
			
class MyLabel(QLabel):

	hovered = pyqtSignal('QObject')
	leaved = pyqtSignal('QObject')
	clicked = pyqtSignal('QObject')

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def enterEvent(self, event):
		self.hovered.emit(self)

	def leaveEvent(self, event):
		self.leaved.emit(self)

	def mouseReleaseEvent(self, event):
		self.clicked.emit(self)