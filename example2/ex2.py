# -*- coding: utf-8 -*-
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class MainWindow(QMainWindow):
	
	def __init__(self):
		super().__init__()
		self.setWindowTitle('Uno')
		self.resize(376, 242)
		self.center()
		self.show()
		Main(self)

	def center(self):
		screen = QDesktopWidget().screenGeometry()
		size = self.geometry()
		self.move((screen.width() - size.width()) / 2,
			(screen.height() - size.height()) / 2)

class Main(QWidget):
	def __init__(self, parent=None):
		self.parent = parent
		super().__init__(parent)
		self.ui = uic.loadUi('layout/main.ui', self)
		self.setWindowTitle('Main')
		self.ui.show()

	@pyqtSlot()
	def enter(self):
		self.setParent(None)
		Second(self.parent)

class Second(QWidget):

	def __init__(self, parent=None):
		self.parent = parent
		super().__init__(parent)
		uic.loadUi('layout/second.ui', self).show()

	@pyqtSlot()
	def back(self):
		self.setParent(None)
		Main(self.parent)

if __name__ == '__main__':
	app = QApplication(sys.argv)
	w = MainWindow()
	sys.exit(app.exec_())