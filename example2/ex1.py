# -*- coding: utf-8 -*-
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Form(QDialog):
	def __init__(self):
		super().__init__()
		self.ui = uic.loadUi('layout/dialog.ui')
		self.ui.show()

if __name__ == '__main__':
	app = QApplication(sys.argv)
	w = Form()
	sys.exit(app.exec_())