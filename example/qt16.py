# -*- coding: utf-8 -*-
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class Communicate(QObject):
	closeApp = pyqtSignal()

class Example(QWidget):
	def __init__(self):
		super().__init__()

		self.init_ui()

	def init_ui(self):
		self.c = Communicate()
		self.c.closeApp.connect(self.close)


		self.setGeometry(300, 300, 250, 150)
		self.setWindowTitle('Emit signal')
		self.show()

	def keyPressEvent(self, e):
		if e.key() == Qt.Key_Escape:
			self.close()

	def mousePressEvent(self, e):
		self.c.closeApp.emit()

if __name__ == '__main__':
	app = QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())