# -*- coding: utf-8 -*-
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class Example(QWidget):
	def __init__(self):
		super().__init__()

		self.init_ui()

	def init_ui(self):
		col = QColor(0, 0, 0)

		self.btn = QPushButton('Dialog', self)
		self.btn.move(20, 20)

		self.btn.clicked.connect(self.showDialog)

		self.frm = QFrame(self)
		self.frm.setStyleSheet("QWidget {background-color: %s}" % col.name())
		self.frm.setGeometry(130, 22, 100, 100)

		self.setGeometry(300, 300, 250, 180)
		self.setWindowTitle('Color dialog')
		self.show()

	def keyPressEvent(self, e):
		if e.key() == Qt.Key_Escape:
			self.close()

	def showDialog(self):
		col = QColorDialog.getColor()

		if col.isValid():
			self.frm.setStyleSheet("QWidget { background-color: %s }" % col.name())
			
if __name__ == '__main__':
	app = QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())