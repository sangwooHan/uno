# -*- coding: utf-8 -*-
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class Example(QMainWindow):
	def __init__(self):
		super().__init__()

		self.init_ui()

	def init_ui(self):
		self.textEdit = QTextEdit()
		self.setCentralWidget(self.textEdit)
		self.statusBar()

		openFile = QAction(QIcon('../images/mushroom.ico'), 'Open', self)
		openFile.setShortcut('Ctrl+O')
		openFile.setStatusTip('Open new file')
		openFile.triggered.connect(self.showDialog)

		menubar = self.menuBar()
		fileMenu = menubar.addMenu('&File')
		fileMenu.addAction(openFile)

		self.setGeometry(300, 300, 350, 300)
		self.setWindowTitle('File dialog')
		self.show()


	def showDialog(self):
		fname = QFileDialog.getOpenFileName(self, 'Open file', '/Users/sangwoo/programming_fundamentals/workspace/project/example')

		if fname[0]:

			with open(fname[0], 'r') as f:
				data = f.read()
				self.textEdit.setText(data)
			
if __name__ == '__main__':
	app = QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())